module "scrapy_asg" {
    source                                          = "..\/..\/modules\/ec2-asg-with-template-module"
    region                                          = var.aws_region
    ecs_cluster_name                                = format("%s-cluster", var.service_name)
    autoscaling_group_name                          = format("%s-asg", var.service_name)
    launch_template_name                            = format("%s-asg-launch-template", var.service_name)
    image_id                                        = var.ecs_optimized_amis[var.aws_region]
    instance_type                                   = var.instance_type
    key_name                                        = var.asg_key_name
    security_groups_ids                             = [data.aws_security_group.xavi-secure-group.id]
    availability_zones                              = var.availability_zones
    subnets_ids                                     = [data.aws_subnet_ids.xavi_public_subnets.ids]
    min_size                                        = var.asg_min_size
    max_size                                        = var.asg_max_size
    desired_size                                    = var.asg_desired_size
    asg_on_demand_allocation_strategy               = var.asg_on_demand_allocation_strategy
    asg_on_demand_base_capacity                     = var.asg_on_demand_base_capacity
    asg_on_demand_percentage_above_base_capacity    = var.asg_on_demand_percentage_above_base_capacity
    asg_spot_allocation_strategy                    = var.asg_spot_allocation_strategy
    asg_spot_instance_pools                         = var.asg_spot_instance_pools
    spot_max_price                                  = var.asg_spot_max_price
    iam_instance_role                               = var.iam_instance_role
    protection_from_scale_in_status                 = var.asg_protection_from_scale_in
    multiple_overrides                              = var.multiple_overrides
}