# Scrappy Showdown Cluster
This folder contains the [Terraform](https://www.terraform.io/) structure that creates and manages the main 
infrastructure that will hold the that will be used to hold `Scrapy project` services.
This infrastructure is composed by three AWS services:

* AWS ECS cluster
* AWS ECS capacity providers  
* AWS EC2 Autoscaling Group
* AWS Application Load Balancer