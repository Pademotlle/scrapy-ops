variable "aws_region" {
  description = "AWS region to operate"
  default     = "eu-west-1"
}

variable "tf_states_s3_bucket" {
  description = "AWS S3 Bucket to store TF states"
  default     = "xavirros-terraform-states"
}

variable "service_name" {
  description = "ECS cluster name"
  default     = "scrapy-showdown"
}

variable "vpc_cidr_block" {
  description = "xavi-vpc cidr block"
  default     = "10.0.0.0/16"
}

variable "availability_zones" {
  description = "The availability zones for the autoscaling group to create instances in."
  default     = "eu-west-1a,eu-west-1b,eu-west-1c"
}

variable "subnet_1_name" {
  description = "VPC subnet name"
  default     = "xavi-public-1"
}

variable "subnet_2_name" {
  description = "VPC subnet name"
  default     = "xavi-public-2"
}

variable "subnet_3_name" {
  description = "VPC subnet name"
  default     = "xavi-public-3"
}

variable "security_group_name" {
  description = "Security group name"
  default     = "xavi-secure-group"
}

variable "cp_managed_termination_protection" {
  description = "Enables or disables container-aware termination of instances in the auto scaling group when scale-in happens"
  default     = "ENABLED"
}

variable "cp_max_scaling_step_size" {
  description = "The maximum step adjustment size. A number between 1 and 10,000. "
  default     = 5
}

variable "cp_step_size" {
  description = "Number of instances that capacity provider may raise up. Limited by the max_scaling_step_size."
  default     = 5
}

variable "cp_min_scaling_step_size" {
  description = "The minimum step adjustment size. A number between 1 and 10,000."
  default     = 1
}

variable "cp_status" {
  description = "Whether auto scaling is managed by ECS. Valid values are ENABLED and DISABLED"
  default     = "ENABLED"
}

variable "cp_target_capacity" {
  description = "The target utilization for the capacity provider. A number between 1 and 100."
  default     = 100
}

variable "ecs_optimized_amis" {
  default = {
    us-east-1      = "ami-05250bd90f5750ed7"
    us-east-2      = "ami-078d79190068a1b35"
    us-west-1      = "ami-0667a9cc6a93f50fe"
    us-west-2      = "ami-06cb61a83c506fe88"
    eu-west-1      = "ami-0c4b9f15bc61a5ba8"
    eu-west-2      = "ami-0e2ab4ec1609a6006"
    eu-central-1   = "ami-00364a85f9634c4f3"
    ap-northeast-1 = "ami-0ca6d4eb36c5aae78"
    ap-southeast-1 = "ami-0167e83338bdf98c2"
    ap-southeast-2 = "ami-095016ddc8e84b54e"
    ca-central-1   = "ami-0761de529c3d697c5"
  }
}
variable "asg_launch_template_path" {
  description = "Relative path where AutoScaling Group launch template is."
  default     = "launch-configuration-scripts/general-services-launch-config.sh"
}

variable "instance_type" {
  description = "The type of image to use on the cluster."
  default     = "t3.nano"
}

variable "asg_key_name" {
  description = "The key name to use for the instances."
  default     = "xavirros"
}

variable "asg_min_size" {
  description = "ECS Autoscaling group minimum size"
  default     = 0
}

variable "asg_max_size" {
  description = "ECS Autoscaling group maximum size"
  default     = 5
}

variable "asg_desired_size" {
  description = "ECS Autoscaling group desired capacity. EC2 instances num that should be running."
  default     = 1
}

variable "spot_price" {
  description = "AWS spot price if it is not defined on demand is used"
  default     = ""
}

variable "iam_instance_role" {
  description = "The attribute name of the IAM instance profile to associate with launched instances."
  default     = "ecsInstanceRole"
}

variable "asg_on_demand_allocation_strategy" {
  description = "Strategy to use when launching on-demand instances"
  default     = "prioritized"
}

variable "asg_on_demand_base_capacity" {
  description = "Absolute minimum amount of desired capacity that must be fulfilled by on-demand instances"
  default     = 0
}

variable "asg_on_demand_percentage_above_base_capacity" {
  description = "Percentage split between on-demand and Spot instances above the base on-demand capacity."
  default     = 100
}

variable "asg_spot_allocation_strategy" {
  description = "How to allocate capacity across the Spot pools."
  default     = "lowest-price"
}

variable "asg_spot_instance_pools" {
  description = "EC2 Auto Scaling selects the cheapest Spot pools and evenly allocates Spot capacity across the number of Spot pools that you specify"
  default     = 2
}

variable "asg_spot_max_price" {
  description = "Maximum price per unit hour that the user is willing to pay for the Spot instances."
  default     = ""
}

variable "asg_protection_from_scale_in" {
  description = "Allows setting instance protection. The autoscaling group will not select instances with this setting for termination during scale in events."
  default     = true
}

variable "multiple_overrides" {
  description = "Auto Scaling considers the order of preference of instance types to launch based on the order specified in the overrides list"
  default     = []
}

variable "protocol" {
  description = "Listener protocol"
  default     = "HTTP"
}

variable "listeners" {
  description = "Map/s with all listeners to add. Port and Protocol shall be specified"
  default = {
    http = {
      port: 80,
      protocol: "HTTP"
    }
  }
}

variable "ssl_listeners" {
  description = "List with all SSL listener names to add"
  default     = {}
}
