resource "aws_ecs_capacity_provider" "scrapy-cluster-capacity-provider" {
  name = format("%s-cp", var.service_name)

  auto_scaling_group_provider {
    auto_scaling_group_arn          = module.scrapy_asg.autoscaling_group_arn
    managed_termination_protection  = var.cp_managed_termination_protection

    managed_scaling {
      maximum_scaling_step_size = var.cp_max_scaling_step_size
      minimum_scaling_step_size = var.cp_min_scaling_step_size
      status                    = var.cp_status
      target_capacity           = var.cp_target_capacity
    }
  }
}