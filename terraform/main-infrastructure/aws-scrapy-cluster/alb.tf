module "scrapy_alb" {
  source                = "..\/..\/modules\/alb-with-listeners-module"
  elb_name              = format("%s-alb", var.service_name)
  subnets_ids           = [data.aws_subnet_ids.xavi_public_subnets.ids]
  security_groups_ids   = [data.aws_security_group.xavi-secure-group.id]
  listeners             = var.listeners
  ssl_listeners         = var.ssl_listeners
}
