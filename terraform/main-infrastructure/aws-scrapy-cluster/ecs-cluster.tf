resource "aws_ecs_cluster" "scrapy_showdown_cluster" {
  name = format("%s-cluster", var.service_name)
}