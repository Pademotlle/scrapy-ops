data aws_vpc "xavi-vpc" {
  cidr_block = var.vpc_cidr_block
}

data "aws_subnet_ids" "xavi_public_subnets" {
  vpc_id = data.aws_vpc.xavi-vpc.id
}

data "aws_security_group" "xavi-secure-group" {
  name = var.security_group_name
}