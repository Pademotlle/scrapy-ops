resource "aws_sns_topic" "disaster_level_wolf" {
  name = var.disaster_level_wolf
}

resource "aws_sns_topic" "disaster_level_tiger" {
  name = var.disaster_level_tiger
}

resource "aws_sns_topic" "disaster_level_demon" {
  name = var.disaster_level_demon
}

resource "aws_sns_topic" "disaster_level_dragon" {
  name = var.disaster_level_dragon
}

resource "aws_sns_topic" "disaster_level_god" {
  name = var.disaster_level_god
}

resource "aws_sns_topic_subscription" "god_level_mail_subscription" {
  topic_arn = aws_sns_topic.disaster_level_god.arn
  protocol  = "email"
  endpoint  = jsondecode(data.aws_secretsmanager_secret_version.account_secret_ver.secret_string)[var.mail]
}

resource "aws_sns_topic_subscription" "dragon_level_mail_subscription" {
  topic_arn = aws_sns_topic.disaster_level_dragon.arn
  protocol  = "email"
  endpoint  = jsondecode(data.aws_secretsmanager_secret_version.account_secret_ver.secret_string)[var.mail]
}