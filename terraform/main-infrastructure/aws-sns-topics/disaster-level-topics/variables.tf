variable "aws_region" {
  description = "AWS region to operate"
  default     = "eu-west-1"
}

variable "tf_states_s3_bucket" {
  description = "AWS S3 Bucket to store TF states"
  default     = "xavirros-terraform-states"
}

variable "disaster_level_wolf" {
  description = "SNS Topic name for a low disaster level: wolf"
  default     = "WOLF"
}

variable "disaster_level_tiger" {
  description = "SNS Topic name for a middle disaster level: tiger"
  default     = "TIGER"
}

variable "disaster_level_demon" {
  description = "SNS Topic name for a high disaster level: demon"
  default     = "DEMON"
}
variable "disaster_level_dragon" {
  description = "SNS Topic name for a critical disaster level: dragon"
  default     = "DRAGON"
}

variable "disaster_level_god" {
  description = "SNS Topic name for deathly disaster level: god"
  default     = "GOD"
}

variable "secret_name" {
  description = "AWS SecretManager secret name"
  default     = "account-secrets"
}

variable "mail" {
  description = "AWS SecretManager secret key for topic target mail"
  default     = "mail"
}