# Disaster level SNS Topics
This folder contains a [Terraform](https://www.terraform.io/) structure that creates and manages some [AWS SNS Topics](https://aws.amazon.com/es/sns/?whats-new-cards.sort-by=item.additionalFields.postDateTime&whats-new-cards.sort-order=desc)
that will be used to define which is the issue criticality for all the `Scrapy project` services.

---

### Levels definition
* `WOLF`: The service is having some low issues that would be nice to be displayed.
* `TIGER`: The service is having some normal issues that don't affect the service performance.
* `DEMON`: The service is having some very important issues that may cause instabilities.
* `DRAGON`: The service is having critical issues that may turn into the service down.
* `GOD`: The service is dead.

--- 
> Taken from [One Punch Man](https://onepunchman.fandom.com/wiki/Category:Disaster_levels) anime.