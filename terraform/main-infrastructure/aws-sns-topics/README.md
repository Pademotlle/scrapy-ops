# AWS SNS Topics
This folder contains a [Terraform](https://www.terraform.io/) structure that creates and manages all [AWS SNS Topics](https://aws.amazon.com/es/sns/?whats-new-cards.sort-by=item.additionalFields.postDateTime&whats-new-cards.sort-order=desc)
that are used for the `Scrapy project`. 

