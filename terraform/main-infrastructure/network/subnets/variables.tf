variable "aws_region" {
  description = "AWS region to operate"
  default     = "eu-west-1"
}

variable "subnet_1_name" {
  description = "Subnet Name"
  default     = "xavi-public-1"
}

variable "subnet_2_name" {
  description = "Subnet Name"
  default     = "xavi-public-2"
}

variable "subnet_3_name" {
  description = "Subnet Name"
  default     = "xavi-public-3"
}

variable "aws_az_a" {
  description = "AZ to operate"
  default     = "eu-west-1a"
}

variable "aws_az_b" {
  description = "AZ to operate"
  default     = "eu-west-1b"
}

variable "aws_az_c" {
  description = "AZ to operate"
  default     = "eu-west-1c"
}

variable "vpc_cidr_block" {
  description = "xavi-vpc cidr block"
  default     = "10.0.0.0/16"
}