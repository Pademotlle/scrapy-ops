resource "aws_subnet" "public-1" {
  vpc_id                  = data.aws_vpc.xavi-vpc.id
  cidr_block              = cidrsubnet(data.aws_vpc.xavi-vpc.cidr_block, 4, 1)
  map_public_ip_on_launch = "true"
  availability_zone       = var.aws_az_a

  tags = {
    Name = var.subnet_1_name
  }
}
resource "aws_subnet" "public-2" {
  vpc_id                  = data.aws_vpc.xavi-vpc.id
  cidr_block              = cidrsubnet(data.aws_vpc.xavi-vpc.cidr_block, 4, 2)
  map_public_ip_on_launch = "true"
  availability_zone       = var.aws_az_b

  tags = {
    Name = var.subnet_2_name
  }
}

resource "aws_subnet" "public-3" {
  vpc_id                  = data.aws_vpc.xavi-vpc.id
  cidr_block              = cidrsubnet(data.aws_vpc.xavi-vpc.cidr_block, 4, 3)
  map_public_ip_on_launch = "true"
  availability_zone       = var.aws_az_c

  tags = {
    Name = var.subnet_3_name
  }
}