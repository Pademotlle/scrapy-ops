variable "aws_region" {
  description = "AWS region to operate"
  default     = "eu-west-1"
}

variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default = "10.0.0.0/16"
}