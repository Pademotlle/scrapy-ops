resource "aws_security_group" "web" {
  name        = "xavi-secure-group"
  description = "Security group for web that allows web traffic from internet to inside"
  vpc_id      = data.aws_vpc.xavi-vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = [jsondecode(data.aws_secretsmanager_secret_version.account_secret_ver.secret_string)[var.xavi_home]]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [jsondecode(data.aws_secretsmanager_secret_version.account_secret_ver.secret_string)[var.xavi_home]]
  }

  tags = {
    Name = "xavi-secure-group"
  }
}