variable "aws_region" {
  description = "AWS region to operate"
  default     = "eu-west-1"
}

variable "vpc_cidr_block" {
  description = "xavi-vpc cidr block"
  default     = "10.0.0.0/16"
}

variable "secret_name" {
  description = "Secret name"
  default     = "account-secrets"
}

variable "xavi_home" {
  description = "Secret key name"
  default     = "xavi-home-ip"
}