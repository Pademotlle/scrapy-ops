variable "aws_region" {
  description = "AWS region to operate"
  default     = "eu-west-1"
}

variable "tf_states_s3_bucket" {
  description = "AWS S3 Bucket to store TF states"
  default     = "xavirros-terraform-states"
}

variable "scrapy_showdown_hosted_zone" {
  description = "This is the name of the hosted zone."
  default     = "scrapyshowdown.cat"
}