# Scrapy project hosted zones
This folder contains a [Terraform](https://www.terraform.io/) structure that creates and manages all [AWS Route 53 hosted zones](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/hosted-zones-working-with.html)
that are used in the `Scrapy project`. 

