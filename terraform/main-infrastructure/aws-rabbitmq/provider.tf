provider "aws" {
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
  region                  = var.aws_region
}

terraform {
  required_version = ">= 0.12.0"
  backend "s3" {
    region = "eu-west-1"
    bucket = "xavirros-terraform-states"
    key    = "scrapy-project/rabbit-mq.tfstate"
  }
}