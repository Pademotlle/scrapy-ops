# Amazon MQ for RabbitMQ cluster
This folder contains a [Terraform](https://www.terraform.io/) structure that creates and manages an [Amazon MQ for 
RabbitMQ](https://aws.amazon.com/es/about-aws/whats-new/2020/11/announcing-amazon-mq-rabbitmq/) message broker and 
handles tasks like provisioning the infrastructure, setting up the broker and updating the software.

This is probably not working because of AWS AmazonMQ for RabbitMQ is very new and it's still having some 
incompatibilities with Terraform.

---
### Accessibility
The Rabbit MQ cluster access will be protected and guaranteed the usage of an AWS secret.
In order to access into the AWS Amazon MQ for RabbitMQ just perform an API call to the AWS SecretManager in order to
obtain the RabbitMQ access credentials:

```text
AWS Secret Name: `amazon-mq/scrapy-rabbit`
AWS Secret Key to get master user name: `rabbit-user`
AWS Secret Key to get master password: `rabbit-pass`
```
