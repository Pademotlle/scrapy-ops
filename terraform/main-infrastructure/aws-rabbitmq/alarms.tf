resource "aws_cloudwatch_metric_alarm" "messages-not-consumed" {
  alarm_name                = format("DRAGON:::alarm:::RabbitMQ:::QueueMessagesNotConsumed", var.disaster_dragon_level)
  comparison_operator       = "GreaterThanThreshold"
  threshold                 = 0
  evaluation_periods        = var.five_minutes_evaluation_period
  datapoints_to_alarm       = var.five_datapoints_to_alarm
  alarm_actions             = [data.aws_sns_topic.dragon_level.arn]
  insufficient_data_actions = [data.aws_sns_topic.god_level.arn]

  metric_query {
    id          = "messages_not_consumed"
    label       = "Amount of queued messages not consumed"
    expression  = "ack_rate==0 AND message_count>0"
    return_data = "true"
  }
  metric_query {
    id = "ack_rate"
    metric {
      metric_name = "AckRate"
      period      = var.one_minute_period
      namespace   = var.namespace
      stat        = var.average_statistic
      dimensions  = {
        "Broker" = var.broker_name
      }
    }
  }
  metric_query {
    id = "message_count"
    metric {
      metric_name = "MessageCount"
      period      = var.one_minute_period
      namespace   = var.namespace
      stat        = var.average_statistic
      dimensions  = {
        "Broker" = var.broker_name
      }
    }
  }
}
