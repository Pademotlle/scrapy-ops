variable "aws_region" {
  description = "AWS region to operate"
  default     = "eu-west-1"
}

variable "tf_states_s3_bucket" {
  description = "AWS S3 Bucket to store TF states"
  default     = "xavirros-terraform-states"
}

variable "broker_name" {
  description = "The broker name"
  default     = "scrapy-rabbit"
}

variable "deployment_mode" {
  description = "The deployment mode of the broker"
  default     = "ACTIVE_STANDBY_MULTI_AZ"
}

variable "engine_type" {
  description = "The broker engine type"
  default     = "RabbitMQ"
}

variable "engine_version" {
  description = "The version of the broker engine"
  default     = "3.8.6"
}

variable "instance_type" {
  description = "The broker's instance type"
  default     = "mq.t3.micro"
}

variable "security_groups" {
  description = "Security group involved IDs list"
  default     = []
}

variable "subnet_ids" {
  description = "Default VPC public subnet ids"
  default     = []
}

variable "secret_name" {
  description = "Secret name from AWS Secret Manager"
  default     = "amazon-mq/scrapy-rabbit"
}

variable "rabbit_user" {
  description = "Secret key to get the rabbit user name"
  default     = "rabbit-user"
}

variable "rabbit_pass" {
  description = "Secret key to get the rabbit password"
  default     = "rabbit-pass"
}

variable "disaster_dragon_level" {
  description = "Topic name for the Disaster level: Dragon"
  default     = "DRAGON"
}

variable "disaster_god_level" {
  description = "Topic name for the Disaster level: God"
  default     = "GOD"
}

variable "namespace" {
  description = "AWS Cloudwatch namespace for the Amazon MQ RabbitMQ service"
  default     = "AWS/AmazonMQ"
}

variable "one_minute_period" {
  description = "Metric period time in seconds"
  default     = 60
}

variable "five_minutes_evaluation_period" {
  description = "Metric evaluation period in minutes"
  default     = 5
}

variable "five_datapoints_to_alarm" {
  description = "Amount of datapoints needed to raise alarm"
  default     = 5
}

variable "average_statistic" {
  description = "Average statistic name"
  default     = "Average"
}