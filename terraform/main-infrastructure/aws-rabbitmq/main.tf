resource "aws_mq_broker" "rabbit_mq" {
  broker_name         = var.broker_name
  deployment_mode     = var.deployment_mode
  engine_type         = var.engine_type
  engine_version      = var.engine_version
  host_instance_type  = var.instance_type
  security_groups     = var.security_groups
  subnet_ids          = var.subnet_ids

  configuration {
    id        = aws_mq_configuration.rabbit_mq_configuration.id
    revision  = aws_mq_configuration.rabbit_mq_configuration.latest_revision
  }
  user {
    username = jsondecode(data.aws_secretsmanager_secret_version.rabbit_secret_ver.secret_string)[var.rabbit_user]
    password = jsondecode(data.aws_secretsmanager_secret_version.rabbit_secret_ver.secret_string)[var.rabbit_pass]
  }
}

resource "aws_mq_configuration" "rabbit_mq_configuration" {
  name            = format("%s-configuration", var.broker_name)
  engine_type     = var.engine_type
  engine_version  = var.engine_version
  data            = ""  # TODO
}