data "aws_secretsmanager_secret" "rabbit_secrets" {
  name = var.secret_name
}

data aws_secretsmanager_secret_version "rabbit_secret_ver" {
  secret_id = data.aws_secretsmanager_secret.rabbit_secrets.id
}

data aws_sns_topic "dragon_level" {
   name = var.disaster_dragon_level
}

data aws_sns_topic "god_level" {
   name = var.disaster_god_level
}