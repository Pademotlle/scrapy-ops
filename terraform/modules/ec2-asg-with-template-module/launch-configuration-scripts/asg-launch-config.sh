#!/bin/bash
echo ECS_CLUSTER=${cluster_name} >> /etc/ecs/ecs.config
echo ECS_AVAILABLE_LOGGING_DRIVERS=[\"json-file\",\"awslogs\"] >> /etc/ecs/ecs.config
echo ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION=10m >> /etc/ecs/ecs.config
echo ECS_IMAGE_CLEANUP_INTERVAL=10m  >> /etc/ecs/ecs.config
echo ECS_IMAGE_MINIMUM_CLEANUP_AGE=30m  >> /etc/ecs/ecs.config
mkdir -p /var/data
curl -X GET -o /tmp/install-newrelic.sh https://s3-eu-west-1.amazonaws.com/we-ec2-amis/install-newrelic.sh
sh /tmp/install-newrelic.sh
