variable "region" {
  description = "The AWS region to create resources in."
}

variable "launch_template_name" {
  description = "The name of the Launch Configuration."
}

variable "image_id" {
  description = "The id of the AMI to use in the cluster."
}

variable "instance_type" {
  description = "The type of image to use on the cluster."
}

variable "key_name" {
  description = "The key name to use for the instances."
}

variable "security_groups_ids" {
  description = "The security groups to assign to the instances."
}

variable "autoscaling_group_name" {
  description = "The name of the security group."
}

variable "availability_zones" {
  description = "The availability zones for the autoscaling group to create instances in."
}

variable "subnets_ids" {
  description = "The list of subnets."
}

variable "min_size" {
  description = "The minimum size for the autoscaling group."
}

variable "max_size" {
  description = "The maximum size for the autoscaling group."
}

variable "desired_size" {
  description = "The desired size for the autoscaling group."
}

variable "spot_price" {
  description = "AWS spot price if it is not defined on demand is used"
  default     = ""
}

variable "iam_instance_role" {
  description = "The attribute name of the IAM instance profile to associate with launched instances."
  default     = "ecsInstanceRole"
}

variable "asg_on_demand_allocation_strategy" {
  description = "Strategy to use when launching on-demand instances"
  default     = "prioritized"
}

variable "asg_on_demand_base_capacity" {
  description = "Absolute minimum amount of desired capacity that must be fulfilled by on-demand instances"
  default     = 0
}

variable "asg_on_demand_percentage_above_base_capacity" {
  description = "Percentage split between on-demand and Spot instances above the base on-demand capacity."
  default     = 100
}

variable "asg_spot_allocation_strategy" {
  description = "How to allocate capacity across the Spot pools."
  default     = "lowest-price"
}

variable "asg_spot_instance_pools" {
  description = "EC2 Auto Scaling selects the cheapest Spot pools and evenly allocates Spot capacity across the number of Spot pools that you specify"
  default     = 2
}

variable "spot_max_price" {
  description = "Maximum price per unit hour that the user is willing to pay for the Spot instances."
  default     = ""
}

variable "protection_from_scale_in_status" {
  description = "Allows setting instance protection. The autoscaling group will not select instances with this setting for termination during scale in events."
}

variable "asg_wait_for_capacity_timeout" {
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out."
  default     = "4m"
}

variable "asg_health_check_grace_period" {
  description = "Time (in seconds) after instance comes into service before checking health."
  default     = 180
}

variable "multiple_overrides" {
  description = "Auto Scaling considers the order of preference of instance types to launch based on the order specified in the overrides list"
  default     = [
      {
        instance_type     = "t3.nano"
        weighted_capacity = 1
      },
  ]
}

variable "asg_launch_template_path" {
  description = "Relative path where AutoScaling Group launch template is."
  default     = "launch-configuration-scripts/asg-launch-config.sh"
}

variable "ecs_cluster_name" {
  description = "ECS cluster name"
}