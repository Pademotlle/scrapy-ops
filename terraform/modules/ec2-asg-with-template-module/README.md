# EC2 Autoscaling group and capacity providers module
This folder contains a [Terraform](https://www.terraform.io/) module that defines an `AutoScaling Group` for EC2/ECS 
service with capacity providers. 

This is intended to be used from the root of each service project by use of a `terraform.tf` file that calls this 
module and defines all its parameters.


## How to use this module?

This folder defines a [Terraform module](https://www.terraform.io/docs/modules/usage.html). It can be used by importing 
the referenced module; just code by adding a `module` configuration and setting its `source` 
parameter to the URL of this folder:

```hcl-terraform
module "ecs_autoscaling_group" {
  # REQUIRED FIELDS
  source = ""
  region = "ue-west-1"
  service_name = "service_name"
  image_id = "ami-xxxxxxx"
  instance_type = "t3.nano"
  key_name = "key"
  autoscaling_group_name = "service_name-asg"
  security_groups_ids = "sg-xxxxxxx"
  availability_zones = "xx-xxxx-xx,yy-yyyy-yy,zz-zzzz-zz"
  subnets_ids = "subnet-xxxxxxx"
  min_size = 1
  max_size = 3
  desired_size = 1
  volume_size = 30
  launch_template_name = "service_name-template"

  # OPTIONAL FIELDS (default values)
  iam_instance_role = "ecsInstanceRole"
  volume_type = "standard"
  spot_price = ""
  # If not defined 'on demand' is used.
  asg_on_demand_allocation_strategy = "prioritized"
  asg_on_demand_base_capacity = 0
  asg_on_demand_percentage_above_base_capacity = 100
  asg_spot_allocation_strategy = "lowest-price"
  asg_spot_instance_pools = 2
  spot_max_price = ""
  # If not defined means the on-demand price.
}
``` 

### Launch Configuration for the AutoScaling Group
In order to define an Autoscaling Group, is necessary to specify its Launch Configuration.
For this reason it's necessary to create a source data that specify where is placed the Launch Configuration Template 
file and the target Cluster.


```hcl-terraform
data "template_file" "launch_configuration" {
  template  = file("${path.module}/launch-configuration-scripts/asg-launch-config.sh")
  vars      = {
    cluster_name = "ecs_cluster_name"
  }
}
```