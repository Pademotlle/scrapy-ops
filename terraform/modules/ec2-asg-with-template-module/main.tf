terraform {
  required_version = ">= 0.12.0"
}

locals {
  mixed_instance = var.asg_on_demand_percentage_above_base_capacity == 100 ? [] : ["mixed_instance"]
  single_instance = length(local.mixed_instance) > 0 ? [] : ["single_instance"]
}

/*
 * Autoscaling group.
 */
resource "aws_autoscaling_group" "ec2_autoscaling_group" {
  name                      = var.autoscaling_group_name
  vpc_zone_identifier       = [var.subnets_ids]
  min_size                  = var.min_size
  max_size                  = var.max_size
  desired_capacity          = var.desired_size
  protect_from_scale_in     = var.protection_from_scale_in_status
  health_check_grace_period = var.asg_health_check_grace_period
  wait_for_capacity_timeout = var.asg_wait_for_capacity_timeout

  dynamic "launch_template" {
    for_each = local.single_instance
    content {
      id      = aws_launch_template.autoscaling_launch_template.id
      version = "$Latest"
    }
  }

  dynamic mixed_instances_policy {
    for_each = local.mixed_instance

    content {
      instances_distribution {
        on_demand_allocation_strategy = var.asg_on_demand_allocation_strategy
        on_demand_base_capacity = var.asg_on_demand_base_capacity
        on_demand_percentage_above_base_capacity = var.asg_on_demand_percentage_above_base_capacity
        spot_allocation_strategy = var.asg_spot_allocation_strategy
        spot_instance_pools = var.asg_spot_instance_pools
        spot_max_price = var.spot_max_price
      }

      launch_template {
        launch_template_specification {
          launch_template_id = aws_launch_template.autoscaling_launch_template.id
          version = "$Latest"
        }

        dynamic override {
          for_each = var.multiple_overrides
          content {
            instance_type     = override.value.instance_type
            weighted_capacity = override.value.weighted_capacity
          }
        }
      }
    }
  }
  lifecycle {
    create_before_destroy = true
    ignore_changes        = [desired_capacity]
  }
  tag {
    key                 = "Project"
    value               = var.autoscaling_group_name
    propagate_at_launch = true
  }
  tag {
    key                 = "costCenter"
    value               = "devs"
    propagate_at_launch = true
  }
  tag {
    key                 = "Name"
    value               = var.autoscaling_group_name
    propagate_at_launch = true
  }
}

/*
 * Autoscaling launch configuration
 */
resource "aws_launch_template" "autoscaling_launch_template" {
  name_prefix             = var.launch_template_name
  image_id                = var.image_id
  instance_type           = var.instance_type
  key_name                = var.key_name
  user_data               = base64encode(data.template_file.launch_config.rendered)
  vpc_security_group_ids  = [var.security_groups_ids]
  iam_instance_profile {
    name = var.iam_instance_role
  }
}