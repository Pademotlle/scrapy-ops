resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = var.service_max_capacity
  min_capacity       = var.service_min_capacity
  resource_id        = "service/${var.ecs_cluster_name}/${var.service_name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "dynamic_cpu_usage" {
  name               = "${var.ecs_cluster_name}:${var.service_name}:${var.metric}"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = var.metric
    }
    target_value        = var.target_value
    scale_in_cooldown   = var.scale_in_cooldown
    scale_out_cooldown  = var.scale_out_cooldown
  }
}