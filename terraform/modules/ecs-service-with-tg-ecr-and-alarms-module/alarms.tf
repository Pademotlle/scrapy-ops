module "service_alarms" {
  source                        = "..\/alarms-module"
  api_alb_name                  = data.aws_alb.load_balancer.name
  api_tg_name                   = aws_alb_target_group.ecs_service_tg.name
  app_autoscaling_min_capacity  = var.service_min_capacity
  ecs_cluster_name              = var.service_name
  ecs_service_name              = var.ecs_cluster_name
}