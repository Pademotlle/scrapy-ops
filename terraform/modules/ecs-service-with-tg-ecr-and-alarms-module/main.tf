/*
 * ECS Service Task definition
 */
resource "aws_ecs_task_definition" "task_definition" {
  family                = var.ecs_task_name
  container_definitions = data.template_file.container_definition.rendered
}

/*
 * ECS Service
 */
resource "aws_ecs_service" "service" {
  name                               = var.service_name
  cluster                            = var.ecs_cluster_name
  task_definition                    = aws_ecs_task_definition.task_definition.id
  desired_count                      = var.ecs_desired_count
  iam_role                           = var.ecs_service_role
  deployment_minimum_healthy_percent = var.minimum_healthy_percent
  health_check_grace_period_seconds  = var.health_check_grace_period

  lifecycle {
    ignore_changes = [
      capacity_provider_strategy
    ]
  }

  dynamic "capacity_provider_strategy" {
    for_each = var.capacity_provider_strategy
    content {
      capacity_provider = capacity_provider_strategy.value.capacity_provider_name
      weight            = capacity_provider_strategy.value.weight
      base              = capacity_provider_strategy.value.base

     }
  }

  load_balancer {
    target_group_arn = aws_alb_target_group
    container_name   = var.container_name
    container_port   = var.alb_container_port
  }
}

/*
 * Cloudwatch log group
 */
resource "aws_cloudwatch_log_group" "log_group" {
  name              = var.ecs_log_group
  retention_in_days = var.log_retention_days

  tags = {
    Application = var.service_name
  }
}

/*
 * ECR repository
 */
resource "aws_ecr_repository" "ecr_repository" {
  name = var.ecr_repository_name
}


/*
 * Target group that forwards traffic to the ECS service from the ALB
 */
resource "aws_alb_target_group" "ecs_service_tg" {
  name        = format("%s-tg", var.service_name)
  port        = var.alb_port
  protocol    = var.alb_tg_protocol
  vpc_id      = var.vpc_id
  target_type = var.alb_tg_target_type

  health_check {
    path    = var.health_check_path
    matcher = var.healthy_status_code
  }
}

/*
 * Listener rule in the target ALB that delivers traffic to the desired target group
 */
resource "aws_alb_listener_rule" "akhq_alb_listener_rule" {
  count = contains(var.listeners_list, "80") ? 1 : 0
  listener_arn        = data.aws_alb_listener.listener.arn

  action {
    type              = var.alb_listener_rule_action_type
    target_group_arn  = aws_alb_target_group.ecs_service_tg.arn
  }

  condition {
    host_header {
      values = split(",", var.listener_rule_paths)
    }
  }
}

/*
 * Alias record bound to the ECS service
 */
resource "aws_route53_record" "route53" {
  count   = var.domain_name_prefix != "" ? 1 : 0
  zone_id = data.aws_route53_zone.route53_zone.id
  name    = format("%s.%s", var.domain_name_prefix, var.hosted_zone)
  type    = "A"

  alias {
    name                   = data.aws_alb.load_balancer.dns_name
    zone_id                = data.aws_alb.load_balancer.zone_id
    evaluate_target_health = false
  }
}