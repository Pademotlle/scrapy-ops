data "aws_ecs_cluster" "cluster" {
  cluster_name = var.ecs_cluster_name
}

data aws_route53_zone "route53_zone" {
  name = var.hosted_zone
}

data aws_alb "load_balancer" {
  name = var.alb_name
}

data aws_alb_listener "listener" {
  load_balancer_arn = data.aws_alb.load_balancer.arn
  port              = 80
}

/* Container definitions */

data "template_file" "container_definition" {
  template = file("${path.module}/tasks-definitions/task-definition.json")
  vars = {
    image_url      = format("%s:latest", aws_ecr_repository.ecr_repository.repository_url)
    container_name = var.container_name
    log_group      = var.ecs_log_group
    region         = var.region
    stream_prefix  = var.stream_prefix
    cpu            = var.cpu
    memory         = var.memory
    container_port = var.container_port
    host_port      = var.host_port
    protocol       = var.port_mapping_protocol
    ulimit_soft    = var.ulimit_soft
    ulimit_hard    = var.ulimit_hard
  }
}
