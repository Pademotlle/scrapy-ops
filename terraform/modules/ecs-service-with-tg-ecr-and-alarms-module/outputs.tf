output "aws_ecs_service_id" {
  value = aws_ecs_service.service.id
}

output "aws_ecr_repository_url" {
  value = aws_ecr_repository.ecr_repository.repository_url
}

output "aws_ecs_task_definition_id" {
  value = aws_ecs_task_definition.task_definition.id
}