output "alb_dns" {
  value = aws_alb.load_balancer.dns_name
}

output "alb_arn_suffix" {
  value = aws_alb.load_balancer.arn_suffix
}

output "alb_arn" {
  value = aws_alb.load_balancer.arn
}

output "alb_zone" {
  value = aws_alb.load_balancer.zone_id
}

output "alb_name" {
  value = aws_alb.load_balancer.name
}