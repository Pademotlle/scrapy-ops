terraform {
  required_version = ">= 0.12.0"
}

/*
 * Create a new Application Load Balancer
 */
resource "aws_alb" "load_balancer" {
  name            = var.elb_name
  internal        = false
  security_groups = split(",", var.security_groups_ids)
  subnets         = split(",", var.subnets_ids)
  idle_timeout    = var.idle_timeout

  tags = {
    Name = var.elb_name
  }
}

/*
 * ALB listener
 */
resource "aws_alb_listener" "listener" {
  for_each = var.listeners
  load_balancer_arn = aws_alb.load_balancer.arn
  port              = lookup(each.value, "port")
  protocol          = lookup(each.value, "protocol")

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "application/json"
      message_body = "{\"status\":\"fail\"}"
      status_code  = "404"
    }
  }
}

/*
 * ALB listener
 */
resource "aws_alb_listener" "ssl_listener" {
  for_each = var.ssl_listeners
  load_balancer_arn = aws_alb.load_balancer.arn
  port              = lookup(each.value, "port")
  protocol          = lookup(each.value, "protocol")
  ssl_policy        = lookup(each.value, "ssl_policy")
  certificate_arn   = lookup(each.value, "default_certificate_arn")

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "application/json"
      message_body = "{\"status\":\"fail\"}"
      status_code  = "404"
    }
  }
}

resource "aws_lb_listener_certificate" "extra_ssl_certificate" {
  for_each        = var.ssl_extra_certificates
  certificate_arn = each.key
  listener_arn    = aws_alb_listener.ssl_listener[lookup(each.value, "listener_name")].arn
}