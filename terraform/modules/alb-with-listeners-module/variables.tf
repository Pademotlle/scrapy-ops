variable "elb_name" {
  description = "The name of the Application Load Balancer."
}

variable "security_groups_ids" {
  description = "The security groups to assign to the instances."
}

variable "subnets_ids" {
  description = "The list of the subnets."
}

variable "sub_domain_name" {
  description = "Sub domain name"
  default = ""
}

variable "idle_timeout" {
  default = 25
}

variable "listeners" {
  description = "Map/s with all listeners to add. Port and Protocol shall be specified"
  default = {
    http = {
      port: 80,
      protocol: "HTTP"
    }
  }
}

variable "ssl_listeners" {
  description = "Map/s with all listeners with SSL to add. 'port', 'protocol', ssl_policy' and 'certificate_arn' shall be specified"
  default = {}
//    {
//      https = {
//      port: 443,
//      protocol: "HTTP"
//      ssl_policy: "ELBSecurityPolicy-2016-08"
//      certificate_arn: data.aws_acm_certificate.cert.arn
//    }
}

variable "ssl_extra_certificates" {
  default = {}
}