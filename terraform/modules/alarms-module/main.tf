/*
 * Disaster level GOD alarms
 */
resource "aws_cloudwatch_metric_alarm" "api_is_down_alarm" {
  alarm_name                = format("%s:::alarm:::%sIsDown", var.disaster_god_level, var.ecs_service_name)
  alarm_description         = "All ${var.alarm_name_prefix} ${var.ecs_service_name} service tasks are down"
  comparison_operator       = "LessThanThreshold"
  threshold                 = 1
  evaluation_periods        = var.one_minute_evaluation_period
  datapoints_to_alarm       = var.one_datapoint_to_alarm
  alarm_actions             = [data.aws_sns_topic.god_level.arn]
  insufficient_data_actions = [data.aws_sns_topic.god_level.arn]
  period                    = var.one_minute_period
  namespace                 = var.ecs_namespace
  metric_name               = "CPUUtilization"
  statistic                 = var.sample_count_statistic
  dimensions                = {
    "ClusterName" = var.ecs_cluster_name
    "ServiceName" = var.ecs_service_name
  }
}

/*
 * Disaster level DRAGON alarms
 */
resource "aws_cloudwatch_metric_alarm" "less_api_hosts_than_minimum" {
  alarm_name                = format("%s:::alarm:::%sIsUnhealthy", var.disaster_dragon_level, var.ecs_service_name)
  alarm_description         = "${title(var.ecs_service_name)} has less tasks than the minimum set"
  comparison_operator       = "LessThanThreshold"
  threshold                 = var.app_autoscaling_min_capacity
  evaluation_periods        = var.one_minute_evaluation_period
  datapoints_to_alarm       = var.one_datapoint_to_alarm
  alarm_actions             = [data.aws_sns_topic.dragon_level.arn]
  insufficient_data_actions = [data.aws_sns_topic.god_level.arn]
  period                    = var.one_minute_period
  namespace                 = var.ecs_namespace
  metric_name               = "CPUUtilization"
  statistic                 = var.sample_count_statistic
  dimensions                = {
    "ClusterName" = var.ecs_cluster_name
    "ServiceName" = var.ecs_service_name
  }
}

/*
 * Disaster level DEMON alarms
 */
resource "aws_cloudwatch_metric_alarm" "api_cpu_usage_is_high" {
  alarm_name                = format("%s:::alarm:::%sCpuUsageIsHigh", var.disaster_demon_level, var.ecs_service_name)
  alarm_description         = "${var.ecs_service_name} service CPU load is very high"
  comparison_operator       = "GreaterThanThreshold"
  threshold                 = 100
  evaluation_periods        = var.five_minute_evaluation_period
  datapoints_to_alarm       = var.five_datapoints_to_alarm
  alarm_actions             = [data.aws_sns_topic.demon_level.arn]
  insufficient_data_actions = [data.aws_sns_topic.god_level.arn]
  period                    = var.one_minute_period
  namespace                 = var.ecs_namespace
  metric_name               = "CPUUtilization"
  statistic                 = var.max_statistic
  dimensions                = {
    "ClusterName" = var.ecs_cluster_name
    "ServiceName" = var.ecs_service_name
  }
}
