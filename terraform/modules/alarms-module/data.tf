data aws_alb "api_alb" {
  name = var.api_alb_name
}

data aws_alb_target_group "api_tg" {
  name = var.api_tg_name
}

data aws_sns_topic "demon_level" {
   name = var.disaster_demon_level
}

data aws_sns_topic "dragon_level" {
   name = var.disaster_dragon_level
}

data aws_sns_topic "god_level" {
   name = var.disaster_god_level
}