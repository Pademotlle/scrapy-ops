# Alarms for ECS service module
This Terraform module grants some predefined alarms for a given ECS service.

### Alarms:
- **API is down** (`GOD`): The whole service is down
- **Hosts unhealthy** (`DRAGON`): There are some hosts that became unhealthy
- **Less hosts than minimum** (`DRAGON`): There are fewer running hosts than the minimum required by the service
- **CPU usage is high** (`DEMON`): The API overall CPU usage reached the 100%
