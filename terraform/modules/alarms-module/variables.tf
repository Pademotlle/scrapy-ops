variable "api_alb_name" {
  description = "ALB used by the API service"
}

variable "api_tg_name" {
  description = "Target group bound to the API service"
}

variable "ecs_cluster_name" {
  description = "ECS cluster name where is placed the API service"
}

variable "ecs_service_name" {
  description = "ECS API service name"
}

variable "disaster_demon_level" {
  description = "Topic name for the Disaster level: Demon"
  default     = "DEMON"
}

variable "disaster_dragon_level" {
  description = "Topic name for the Disaster level: Dragon"
  default     = "DRAGON"
}

variable "disaster_god_level" {
  description = "Topic name for the Disaster level: God"
  default     = "GOD"
}

variable "sample_count_statistic" {
  description = "Statistic name for SampleCount"
  default     = "SampleCount"
}

variable "average_statistic" {
  description = "Statistic name for Average"
  default     = "Average"
}

variable "sum_statistic" {
  description = "Statistic name for Sum"
  default     = "Sum"
}

variable "max_statistic" {
  description = "Statistic name for Maximum"
  default     = "Maximum"
}

variable "p99_statistic" {
  description = "p99 statistic name"
  default     = "p99.00"
}

variable "one_datapoint_to_alarm" {
  description = "Needed 1 datapoint in metric to raise alarm"
  default     = 1
}

variable "five_datapoints_to_alarm" {
  description = "Needed 5 datapoints in metric to raise alarm"
  default     = 5
}

variable "one_minute_period" {
  description = "Metric period time in seconds"
  default     = 60
}

variable "one_minute_evaluation_period" {
  description = "Metric evaluation period time in minutes"
  default     = 1
}

variable "five_minute_evaluation_period" {
  description = "Metric evaluation period time in minutes"
  default     = 5
}

variable "ecs_namespace" {
  description = "Namespace for ECS"
  default     = "AWS/ECS"
}

variable "alb_namespace" {
  description = "Namespace for ALB"
  default     = "AWS/ApplicationELB"
}

variable "app_autoscaling_min_capacity" {
  description = "Minimum value set in the ECS service app-autoscaling policy"
}

variable "high_p99_response_time" {
  description = "Max threshold value for an acceptable P99 target response time"
  default     = 0.5
}

variable "high_response_time" {
  description = "Threshold that indicates that the target response time is high"
  default     = 0.5
}

variable "very_high_response_time" {
  description = "Threshold that indicates that the target response time is very high"
  default     = 1
}

variable "alarm_name_prefix" {
  description = "An optional prefix to add to the alarm name"
  default     = ""
}

variable "intermitent_data" {
  description = "If the API may not receive traffic during a while, so it receives intermittent requests"
  default     = false
}