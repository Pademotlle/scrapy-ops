# scrapy-ops
This project contains part of the operations and the needed infrastructure to support all Scrapy project 
modules.

--- 
### Project overview
The target of this project is to create an infrastructure that collects data from [PokemonShowdown](https://play.pokemonshowdown.com/)
battles for a concrete battle mode and then make data accessible from an API and show it in a Frontend (TBD).

There are some modules that are pending to do or are in progress, but the production structure will be something like: 
<br></br>
<br></br>

![image info](./docs/graph.png)

---
> TODO: Finish CI and CD with Ansible + Terraform